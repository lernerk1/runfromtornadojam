using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TwoDController : MonoBehaviour
{
    [SerializeField] private LayerMask groundLayers;
    [SerializeField] private Animator animator;

    [SerializeField] private Rigidbody rigidbody;
    [SerializeField] private float forceScalar;

    [SerializeField] private Transform GroundCheckorigin;

    private bool isJumping = false;
    private bool canJump = true;
    private void Update()
    {
        
        bool spaceHeld = Input.GetKeyDown(KeyCode.Space);
      

        if (!isJumping && canJump && spaceHeld)
        {
            Jump();
        }
        
        
    }

    private void FixedUpdate()
    {
        GroundCheck();
    }

    void GroundCheck()
    {
        bool b = Physics.Raycast(GroundCheckorigin.position, -Vector3.up, 0.1f, groundLayers);
        canJump = b;
        if (canJump)
        {
            Debug.Log("works");
        }
        if (isJumping && canJump)
        {
            isJumping = false;
        }
        if (isJumping == false)
        {
            Debug.Log("jump");
           
        }
    }
    void Jump()
    {
        rigidbody.AddForce(Vector3.up * forceScalar);
        isJumping = true;
        canJump = false;
        animator.SetTrigger("Jump");
    }

    private void OnDrawGizmos()
    {
        Debug.DrawLine(GroundCheckorigin.position, GroundCheckorigin.position + Vector3.down);
    }
}
