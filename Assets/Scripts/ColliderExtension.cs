using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ColliderExtension : MonoBehaviour
{
    public UnityEvent OnEnter;
    public UnityEvent OnStay;
    public UnityEvent OnExit;

    private void OnCollisionEnter(Collision collision)
    {

        OnEnter?.Invoke();
    }

    private void OnCollisionStay(Collision collision)
    {
        OnStay?.Invoke();
    }

    private void OnCollisionExit(Collision collision)
    {
        OnExit?.Invoke();
    }
}
