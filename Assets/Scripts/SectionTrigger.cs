using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SectionTrigger : MonoBehaviour
{
    //start road section
    public GameObject roadStart;

    //spawn position
    public Transform spawnPoint;

    //road section prefab
    private GameObject roadSection;

    //road parent object
    public Transform roadParent;

    //list of road section prefab
    public List<GameObject> roadSectionList;

    private void OnTriggerEnter(Collider col)
    {
        //random number to pick from list
        int r = Random.Range(0, roadSectionList.Count);
        //assign roadSection
        roadSection = roadSectionList[r];

        //if player collides with CreateTrigger
        if (col.gameObject.CompareTag("Create"))
        {
            //create roadSection as a child of roadParent
            Instantiate(roadSection, spawnPoint.position, Quaternion.identity, roadParent);
        }

    //   //if player collides with an obstacle
    //    if (col.gameObject.CompareTag("Obstacle"))
    //    {
    //        //make a list of all the roads in the scene
    //        GameObject[] roads = GameObject.FindGameObjectsWithTag("Road");

    //        //destroy them
    //        foreach (GameObject road in roads)
    //        {
    //            Destroy(road);
    //        }

    //        //create a new start road
    //        Instantiate(roadStart, new Vector3(0, 0, 0), Quaternion.identity, roadParent);
    //    }
    }
}
