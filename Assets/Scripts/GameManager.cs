using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public void LoadLevelScene() => SceneManager.LoadScene("_Scene_0");

    public void ResetHighScore() => PlayerPrefs.SetInt("HighScore", 0);
}
