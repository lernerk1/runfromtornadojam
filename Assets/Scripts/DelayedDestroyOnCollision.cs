using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DelayedDestroyOnCollision : MonoBehaviour
{
    public float delay = 1f;

    public void DelayedDestroy()
    {
        Invoke(nameof(DestroyThis), delay);
    }

    public void DestroyThis()
    {
        Destroy(gameObject);
    }
}
