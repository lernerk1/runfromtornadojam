using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingObject : MonoBehaviour
{
    public float width = 1f;
    public float speed = 0.1f;

    private float leftBoundX;
    private float rightBoundX;
    private bool left = false;
    // Start is called before the first frame update
    void Start()
    {
        leftBoundX = transform.position.x - width;
        rightBoundX = transform.position.x + width;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (left) 
        {
            transform.position = new Vector3(transform.position.x - speed, transform.position.y, transform.position.z);
            if (transform.position.x <= leftBoundX) 
            {
                left = false;
            }
        }
        else
        {
            transform.position = new Vector3(transform.position.x + speed, transform.position.y, transform.position.z);
            if (transform.position.x >= rightBoundX)
            {
                left = true;
            }
        }
    }
}
