using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour
{
    //speed of the road pieces
    [SerializeField]
    private float speed = -2;

    [SerializeField]
    private float length, startpos;

    public 

    // Update is called once per frame
    void Update()
    {
        //move the road
        transform.position += new Vector3(speed, 0, 0) * Time.deltaTime;

    }

    private void OnTriggerEnter(Collider col)
    {
        //if colliding with destroy trigger
        if (col.gameObject.CompareTag("Destroy"))
        {
            //destroy
            //Destroy(gameObject);
            //Debug.Log("COLLIISON WITH DESTROY");

            transform.position = new Vector3(startpos, transform.position.y, transform.position.z);

        }

        

    }
}
