using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DeathPlane : MonoBehaviour
{
    public StringSO SceneName;

    private void OnTriggerEnter (Collider other) {

        if (other.CompareTag("Player")) {

            SceneManager.LoadScene(SceneName.SceneName);
            
        }

    }

}
