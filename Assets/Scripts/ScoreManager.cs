using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManager : MonoBehaviour
{
    public static ScoreManager Instance;

    public int Score { get; private set; } = 0;
    public int HighScore { get => PlayerPrefs.GetInt(nameof(HighScore)); private set => PlayerPrefs.SetInt(nameof(HighScore), value); }
    
    // Start is called before the first frame update
    void Start()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
        }
    }

    public void AddScore()
    {
        Score += 1;
    }

    public void CheckForNewHighScore()
    {
        if (Score > HighScore)
        {
            NewHighScore();
        }
    }

    private void NewHighScore()
    {
        HighScore = Score;
    }

    public void ResetHighScore()
    {
        HighScore = 0;
    }
}
