using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SceneName", menuName = "ScriptableObjects/SceneName")]
public class StringSO : ScriptableObject
{
    public string SceneName;
}
