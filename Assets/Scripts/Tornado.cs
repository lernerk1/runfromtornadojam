using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tornado : MonoBehaviour
{
    public Transform[] spawnPoints;

    public float acceleration = 3f;
    public float maxSpeed = 5f;

    public GameObject debrisPrefab;

    private Rigidbody rigid;

    public float debrisSpawnCooldown = 4f;


    private void Start() {
        rigid = GetComponent<Rigidbody>();
        StartCoroutine(SpawnDebris());
    }

    private void FixedUpdate() {

        if (rigid.velocity.x < maxSpeed) {
            rigid.AddForce(new Vector2(acceleration, 0));
        }

    }

    private IEnumerator SpawnDebris() {

        float timer = Time.time + debrisSpawnCooldown;

        GameObject debrisInstance = Instantiate(debrisPrefab, spawnPoints[Random.Range(0, spawnPoints.Length)].position, Quaternion.identity);
        float forcePower = Random.Range(maxSpeed / 3, maxSpeed);

        debrisInstance.GetComponent<Rigidbody>().AddForce(Vector2.right * forcePower, ForceMode.Impulse);

        while (Time.time <= timer) {

            yield return null;
        }

        StartCoroutine(SpawnDebris());

    }

}
